const test = require('ava');
const users = require('../../../lib/seed/users');
const { apiV1 } = require('../../common');

test.serial('/users/:id - get nominal', async (t) => {
  const { agent } = t.context;
  const user = users.documents[0];
  const userId = user._id.toString();
  const res = await agent.get(apiV1(`users/${userId}`));
  t.is(res.statusCode, 200);
  t.is(res.body.login, user.login);
});

test.serial('/users/:id - get with user does not exist', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const res = await agent.get(apiV1(`users/${fakeId}`));
  t.is(res.statusCode, 404);
});
