const test = require('ava');
const chapters = require('../../../lib/seed/chapters');
const { apiV1 } = require('../../common');

test.serial('/chapters/:id - link direct nominal', async (t) => {
  const { agent } = t.context;
  let res;
  const id = chapters.documents[0]._id.toString();
  const nextId = chapters.documents[1]._id.toString();

  res = await agent.get(apiV1(`chapters/${id}`));
  t.is(res.statusCode, 200);
  t.is(res.body.next, undefined);

  const link = { chapter: nextId };
  res = await agent.patch(apiV1(`chapters/${id}`)).send(link);
  t.is(res.statusCode, 200);
  t.is(res.statusCode, 200);
  t.is(res.body, true);

  res = await agent.get(apiV1(`chapters/${id}`));
  t.is(res.statusCode, 200);
  t.deepEqual(res.body.next, link);
});

test.serial('/chapters/:id - link direct with chapter does not exist', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const nextId = chapters.documents[1]._id.toString();

  const link = { chapter: nextId };
  const res = await agent.patch(apiV1(`chapters/${fakeId}`)).send(link);
  t.is(res.statusCode, 404);
});

test.serial('/chapters/:id - link direct with linked chapter does not exist', async (t) => {
  const { agent } = t.context;
  const id = chapters.documents[0]._id.toString();
  const fakeId = '111111111111111111111111';
  const link = { chapter: fakeId };
  const res = await agent.patch(apiV1(`chapters/${id}`)).send(link);
  t.is(res.statusCode, 400);
  t.is(res.body.message, 'Linked chapter does not exist.')
});

test.serial('/chapters/:id - link choice nominal', async (t) => {
  const { agent } = t.context;
  let res;
  const id = chapters.documents[0]._id.toString();
  const nextId1 = chapters.documents[1]._id.toString();
  const nextId2 = chapters.documents[2]._id.toString();

  res = await agent.get(apiV1(`chapters/${id}`));
  t.is(res.statusCode, 200);
  t.is(res.body.next, undefined);

  const choice = { choices: [
    { text: 'Choice 1', chapter: nextId1 },
    { text: 'Choice 2', chapter: nextId2 },
  ] };
  res = await agent.patch(apiV1(`chapters/${id}`)).send(choice);
  t.is(res.statusCode, 200);
  t.is(res.body, true);

  res = await agent.get(apiV1(`chapters/${id}`));
  t.is(res.statusCode, 200);
  t.deepEqual(res.body.next, choice);
});

test.serial('/chapters/:id - link choice with chapter does not exist', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const nextId1 = chapters.documents[1]._id.toString();
  const nextId2 = chapters.documents[2]._id.toString();

  const choice = { choices: [
    { text: 'Choice 1', chapter: nextId1 },
    { text: 'Choice 2', chapter: nextId2 },
  ] };
  const res = await agent.patch(apiV1(`chapters/${fakeId}`)).send(choice);
  t.is(res.statusCode, 404);
});

test.serial('/chapters/:id - link choice with linked chapters does not exist', async (t) => {
  const { agent } = t.context;
  const id = chapters.documents[0]._id.toString();
  const nextId = chapters.documents[1]._id.toString();
  const fakeId = '111111111111111111111111';

  const choice = { choices: [
    { text: 'Choice 1', chapter: nextId },
    { text: 'Choice 2', chapter: fakeId },
  ] };
  const res = await agent.patch(apiV1(`chapters/${id}`)).send(choice);
  t.is(res.statusCode, 400);
  t.is(res.body.message, 'Some linked chapters does not exist.');
});

test.serial('/chapters/:id - link choice with too low choices', async (t) => {
  const { agent } = t.context;
  const id = chapters.documents[0]._id.toString();
  const nextId = chapters.documents[1]._id.toString();

  const choice = { choices: [
    { text: 'Choice 1', chapter: nextId },
  ] };
  const res = await agent.patch(apiV1(`chapters/${id}`)).send(choice);
  t.is(res.statusCode, 400);
});

