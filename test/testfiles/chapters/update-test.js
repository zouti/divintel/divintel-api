const test = require('ava');
const chapters = require('../../../lib/seed/chapters');
const { apiV1 } = require('../../common');

test.serial('/chapters/:id - update nominal', async (t) => {
  const { agent } = t.context;
  let res;
  const chapter = chapters.documents[0];
  const id = chapter._id.toString();
  const updatedChapter = {
    title: 'Test',
    text: 'A text to test.',
    published: true,
  };

  res = await agent.put(apiV1(`chapters/${id}`)).send(updatedChapter);
  t.is(res.statusCode, 200);
  t.is(res.body, true);

  res = await agent.get(apiV1(`chapters/${id}`));
  t.is(res.statusCode, 200);
  t.is(res.body.title, updatedChapter.title);
  t.is(res.body.text, updatedChapter.text);
  t.is(res.body.published, updatedChapter.published);
});

test.serial('/chapters/:id - update with chapter does not exist', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const updatedChapter = {
    title: 'Test',
    text: 'A text to test.',
    published: true,
  };

  const res = await agent.put(apiV1(`chapters/${fakeId}`)).send(updatedChapter);
  t.is(res.statusCode, 404);
});
