const test = require('ava');
const tales = require('../../../lib/seed/tales');
const { apiV1 } = require('../../common');

test.serial('/chapters - create nominal', async (t) => {
  const { agent } = t.context;
  const tale = tales.documents[0];
  const taleId = tale._id.toString();
  const chapter = {
    title: 'Test',
    text: 'A text to test.',
    tale: taleId
  };
  const res = await agent.post(apiV1('chapters')).send(chapter);
  t.is(res.statusCode, 200);
  t.is(res.body.title, chapter.title);
  t.is(res.body.text, chapter.text);
  t.is(res.body.published, false);
});

test.serial('/chapters - create with tale does not exists', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const chapter = {
    title: 'Test',
    text: 'A text to test.',
    tale: fakeId
  };
  const res = await agent.post(apiV1('chapters')).send(chapter);
  t.is(res.statusCode, 400);
  t.is(res.body.message, 'Tale does not exist.');
});