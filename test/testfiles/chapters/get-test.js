const test = require('ava');
const chapters = require('../../../lib/seed/chapters');
const { apiV1 } = require('../../common');

test.serial('/chapters/:id - get nominal', async (t) => {
  const { agent } = t.context;
  const chapter = chapters.documents[0];
  const id = chapter._id.toString();
  const res = await agent.get(apiV1(`chapters/${id}`));
  t.is(res.statusCode, 200);
  t.is(res.body.title, chapter.title);
  t.is(res.body.text, chapter.text);
});

test.serial('/chapters/:id - get with chapter does not exist', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const res = await agent.get(apiV1(`chapters/${fakeId}`));
  t.is(res.statusCode, 404);
});