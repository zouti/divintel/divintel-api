const test = require('ava');
const chapters = require('../../../lib/seed/chapters');
const { apiV1 } = require('../../common');

test.serial('/chapters/:id - delete nominal', async (t) => {
  const { agent } = t.context;
  let res;
  const chapter = chapters.documents[0];
  const id = chapter._id.toString();

  res = await agent.get(apiV1(`chapters/${id}`));
  t.is(res.statusCode, 200);

  res = await agent.delete(apiV1(`chapters/${id}`));
  t.is(res.statusCode, 200);
  t.is(res.body, true);

  res = await agent.get(apiV1(`chapters/${id}`));
  t.is(res.statusCode, 404);
});

test.serial('/chapters/:id - delete with chapter does not exist', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const res = await agent.delete(apiV1(`chapters/${fakeId}`));
  t.is(res.statusCode, 404);
});
