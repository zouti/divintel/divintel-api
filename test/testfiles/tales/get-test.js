const test = require('ava');
const tales = require('../../../lib/seed/tales');
const users = require('../../../lib/seed/users');
const { apiV1 } = require('../../common');

test.serial('/tales/:id - get nominal', async (t) => {
  const { agent } = t.context;
  const user = users.documents[0];
  const tale = tales.documents[2];
  const taleId = tale._id.toString();
  const res = await agent.get(apiV1(`tales/${taleId}`));
  t.is(res.statusCode, 200);
  t.is(res.body.name, tale.name);
  t.is(res.body.description, tale.description);
  t.is(typeof res.body.chapters, 'object');
  t.is(typeof res.body.chapters.next.chapter, 'object');
  t.is(res.body.chapters.next.chapter.next.choices.length, 2);
  t.is(typeof res.body.chapters.next.chapter.next.choices[0].chapter, 'object');
  t.is(typeof res.body.chapters.next.chapter.next.choices[1].chapter, 'object');
  t.is(res.body.chapters.next.chapter.next.choices[0].chapter.next, undefined);
  t.is(typeof res.body.chapters.next.chapter.next.choices[1].chapter.next.chapter, 'object');
  t.is(res.body.author.login, user.login);
});

test.serial('/tales/:id - get with tale does not exist', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const res = await agent.get(apiV1(`tales/${fakeId}`));
  t.is(res.statusCode, 404);
});