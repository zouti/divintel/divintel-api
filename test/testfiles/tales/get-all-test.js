const test = require('ava');
const { apiV1 } = require('../../common');

test.serial('/tales - get all nominal', async (t) => {
  const { agent } = t.context;

  const res = await agent.get(apiV1('tales'));
  t.is(res.statusCode, 200);
  t.is(res.body.length, 3);
  t.true(res.body.every(tale => tale.author));
});