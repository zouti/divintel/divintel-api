const test = require('ava');
const { apiV1 } = require('../../common');

test.serial('/tales - create nominal', async (t) => {
  const { agent } = t.context;
  const tale = {
    title: 'Test',
    description: 'A description to test.',
  };
  const res = await agent.post(apiV1('tales')).send(tale);
  t.is(res.statusCode, 200);
  t.true(!!res.body._id);
  t.is(res.body.title, tale.title);
  t.is(res.body.description, tale.description);
  t.is(res.body.ended, false);
});
