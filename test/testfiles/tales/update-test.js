const test = require('ava');
const tales = require('../../../lib/seed/tales');
const { apiV1 } = require('../../common');

test.serial('/tales/:id - update nominal', async (t) => {
  const { agent } = t.context;
  let res;
  const tale = tales.documents[0];
  const id = tale._id.toString();

  const updatedTale = {
    title: 'Test',
    description: 'A description to test.',
    ended: true,
  };

  res = await agent.put(apiV1(`tales/${id}`)).send(updatedTale);
  t.is(res.statusCode, 200);
  t.is(res.body, true);

  res = await agent.get(apiV1(`tales/${id}`));
  t.is(res.statusCode, 200);
  t.is(res.body.title, updatedTale.title);
  t.is(res.body.description, updatedTale.description);
  t.is(res.body.ended, updatedTale.ended);
});

test.serial('/tales/:id - update with tale does not exist', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const res = await agent.put(apiV1(`tales/${fakeId}`)).send({ title: 'Test' });
  t.is(res.statusCode, 404);
});
