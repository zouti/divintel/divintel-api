const test = require('ava');
const tales = require('../../../lib/seed/tales');
const { apiV1 } = require('../../common');

test.serial('/tales/:id - delete nominal', async (t) => {
  const { agent } = t.context;
  let res;
  const tale = tales.documents[0];
  const id = tale._id.toString();

  res = await agent.get(apiV1(`tales/${id}`));
  t.is(res.statusCode, 200);

  res = await agent.delete(apiV1(`tales/${id}`));
  t.is(res.statusCode, 200);
  t.is(res.body, true);

  res = await agent.get(apiV1(`tales/${id}`));
  t.is(res.statusCode, 404);
});

test.serial('/tales/:id - delete with tale does not exist', async (t) => {
  const { agent } = t.context;
  const fakeId = '111111111111111111111111';
  const res = await agent.delete(apiV1(`tales/${fakeId}`));
  t.is(res.statusCode, 404);
});
