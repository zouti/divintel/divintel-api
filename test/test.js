const { join } = require('path');
const test = require('ava');
const request = require('supertest');
const globby = require('globby');
const { MongoMemoryServer } = require('mongodb-memory-server');
const config = require('../config/config');
const dbClient = require('../lib/db/db_client');
const { createServer } = require('../lib/server');
const { setup } = require('../lib/seed/seed');

let server;
let mongoServer;
test.before(async () => {
  mongoServer = new MongoMemoryServer({ binary: { version: '3.4.14' } });
  const dbUrl = await mongoServer.getUri();
  const dbName = await mongoServer.getDbName();
  await dbClient.connect(dbUrl, dbName);
  config.host = 'localhost';
  server = await createServer();
});

test.beforeEach(async (t) => {
  await setup();
  t.context = {
    agent: request.agent(server),
  };
});

test.after(() => mongoServer.stop());

/* Load test files */
for (const file of globby.sync(join(__dirname, 'testfiles/**/*-test.js'))) {
  require(file);
}
