module.exports = {
  apiV1(path) {
    return `/api/v1/${path}`;
  },
};
