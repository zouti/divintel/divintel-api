module.exports = {
  host: process.env.HOST || '0.0.0.0',
  port: process.env.PORT || 8080,
  db: {
    url: process.env.DATABASE_URL || 'mongodb://mongo',
    name: process.env.DATABASE_NAME || 'divintel',
  },
};
