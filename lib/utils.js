module.exports = {
  isDefined: value => value !== null && value !== undefined,
};