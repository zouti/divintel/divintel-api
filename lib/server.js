const Fastify = require('fastify');
const { HttpError } = require('http-errors');
const config = require('../config/config');
const registerSchemas = require('../api/routes/schemas');
const registerRoutes = require('../api/routes');

module.exports = {
  createServer() {
    return new Promise((resolve, reject) => {
      const fastify = Fastify({
        logger: {
          prettyPrint: {
            translateTime: true,
            colorize: true,
            ignore: 'pid,hostname,reqId,responseTime,req,res',
            messageFormat: '{req.method} {req.url} [id={reqId}] {msg} ',
          },
        },
      })

      registerSchemas(fastify);
      registerRoutes(fastify);

      fastify.setErrorHandler((error, request, reply) => {
        if (error instanceof HttpError) {
          reply.code(error.status).send({ message: error.message });
        } else if (error.validation) {
          reply.code(400).send({ message: error.message });
        } else {
          reply.code(500).send({ message: 'Internal Server Error' });
        }
      })

      fastify.listen(config.port, config.host, function (err, address) {
        if (err) {
          fastify.log.error(err);
          reject(err);
        }
        resolve(fastify.server);
      })
    })
  },
};
