const ObjectId = require('mongodb').ObjectId;

module.exports = {
  collection: 'chapters',
  documents: [

    /* Harry Potter */
    {
      _id: new ObjectId('609e613e50e64eb452b3f9d4'),
      title: 'Le survivant',
      text: '{"type":"doc","content":[{"type":"paragraph","content":[{"type":"text","text":"Mr et Mrs Dursley, qui habitaient au 4, Privet Drive, avaient toujours affirmé avec la plus grande fierté qu\'ils étaient parfaitement normaux. Jamais quiconque n\'aurait imaginé qu\'ils puissent se trouver impliqués dans quoi que ce soit d\'étrange ou de mystérieux. Mr Dursley dirigeait la Grunnings, une entreprise qui fabriquait des perceuses. C\'était un homme grand et massif, qui n\'avait pratiquement pas de cou, mais possédait en revanche une moustache de belle taille. Mrs Dursley, quant à elle, était mince et blonde et disposait d\'un cou deux fois plus long que la moyenne, ce qui lui était fort utile pour espionner ses voisins en regardant par-dessus les clôtures des jardins. Les Dursley avaient un petit garçon prénommé Dudley et c\'était à leurs yeux le plus bel enfant du monde. Les Dursley avaient tout ce qu\'ils voulaient. La seule chose indésirable qu\'ils possédaient, c\'était un secret dont ils craignaient plus que tout qu\'on le découvre un jour. Si jamais quiconque venait à entendre parler des Potter, ils étaient convaincus qu\'ils ne s\'en remettraient pas. Mrs Potter était la sœur de Mrs Dursley, mais toutes deux ne s\'étaient plus revues depuis des années. En fait, Mrs Dursley faisait comme si elle était fille unique, car sa sœur et son bon à rien de mari étaient aussi éloignés que possible de tout ce qui faisait un Dursley. [...] Un homme apparut à l\'angle de la rue que le chat avait observé pendant tout ce temps. On n\'avait encore jamais vu dans Privet Drive quelque chose qui ressemblât à cet homme. Il était grand, mince et très vieux, à en juger par la couleur argentée de ses cheveux et de sa barbe qui lui descendaient jusqu\'à la taille. Il était vêtu d\'une longue robe, d\'une cape violette qui balayait le sol et chaussé de bottes à hauts talons munies de boucles. Ses yeux bleus et brillants étincelaient derrière des lunettes en demi-lune et son long nez crochu donnait l\'impression d\'avoir été cassé au moins deux fois. Cet homme s\'appelait Albus Dumbledore. Il prit une sorte de briquet en argent dans sa poche et éteignit toutes les lumières de la rue avec puis il se rendit au numéro 4."}]}]}',
      tale: new ObjectId('5fd6498973b381029736b561'),
      published: false,
    },
    {
      _id: new ObjectId('60a0011575a17901e25ec552'),
      title: 'Une vitre disparaît',
      text: '{"type":"doc","content":[{"type":"paragraph","content":[{"type":"text","text":"Dix ans plus tard, chez les Dursley, Harry a grandi en vivant dans le placard sous l\'escalier et en étant maltraité par son oncle, sa tante et son cousin. [...] Harry avait toujours été petit et maigre pour son âge. Il paraissait d\'autant plus petit qu\'il portait les vieux vêtements de son cousin Dudley qui était à peu près quatre fois plus gros que lui. Harry avait un visage mince, des cheveux noirs et des yeux verts. Il portait des lunettes rondes qu\'il avait fallu rafistoler avec du papier adhésif à cause des nombreux coups de poing de son cousin. Ce jour-là c\'est l\'anniversaire de Dudley et comme chaque année, son cousin est couvert de cadeaux. Cette année encore, Harry devait aller chez une voisine pendant que le reste de la famille accompagné d\'un ami de Dudley faisait une sortie pour fêter l’événement. Malheureusement pour les Dursley, la voisine s\'est cassée la jambe et ils se voient dans l\'obligation d\'amener Harry avec eux au zoo. Là-bas, Harry fait la rencontre extraordinaire d\'un serpent et se surprend à parler avec lui ; mais il n\'est pas le seul à s\'en rendre compte ; l\'ami de Dudley en parle aux Dursley. Dudley, jaloux de son cousin, donne des coups de poings à Harry. C\'est à cet instant que la vitre qui retenait le serpent disparaît, le serpent s\'enfuit et Dudley se retrouve enfermé à sa place.."}]}]}',
      tale: new ObjectId('5fd6498973b381029736b561'),
      published: false,
    },
    {
      _id: new ObjectId('60a006b4a70ea1cab77b4629'),
      title: 'Les lettres de nulle part',
      text: '{"type":"doc","content":[{"type":"paragraph","content":[{"type":"text","text":"Un matin, en allant prendre le courrier, Harry découvrit une lettre sur le paillasson sur laquelle était écrit : Mr. Harry Potter Dans le placard sous l\'escalier 4, Privet Drive Little Whinging Surrey L\'enveloppe, lourde et épaisse, était faite d\'un parchemin jaune et l\'adresse était écrite à l\'encre vertémeraude. Il n\'y avait pas de timbre. En retournant l\'enveloppe, Harry vit un sceau de cire frappé d\'un écusson qui représentait un aigle, un lion, un blaireau et un serpent entourant la lettre « P ». Harry s\'apprêtait à ouvrir l\'enveloppe lorsque son oncle la lui confisqua d\'un air inquiet. Le lendemain, la lettre arriva à nouveau mais en dix exemplaires. Il en arriva de plus en plus chaque joursans que quelqu\'un ne sache comment elles pouvaient arriver sans timbre. Mr Dursley finit même par clouer des planches sur la boîte aux lettres. Malheureusement pour lui, le lendemain, les lettres trouvèrent un autre moyen d\'entrer dans la maison puisqu\'elles avaient été roulées et mises dans des boîtes à œufs. Le jour suivant ce fut encore pire : les enveloppes arrivèrent par dizaines en s\'échappant de la cheminée. Les Dursley avaient réussi in extremis à empêcher Harry d\'attraper une lettre et décidèrent de partirse réfugier dans une petite cabane sur une île. Cependant, ce départ ne suffit pas puisqu\'un soir, alors qu\'une violente tempête éclatait dehors, quelqu\'un se mit à frapper à la porte faisant trembler les murs..."}]}]}',
      tale: new ObjectId('5fd6498973b381029736b561'),
      published: false,
    },

    /* Divintel's tale */
    {
      _id: new ObjectId('60a019f274d44dff9aca6c20'),
      title: 'Lorem ipsum',
      text: '{"type":"doc","content":[{"type":"paragraph","content":[{"type":"text","text":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}]}]}',
      tale: new ObjectId('60a0188367e1c2e07fa5201d'),
      published: true,
      next: {
        chapter: new ObjectId('60a019faa13450d1025f8041'),
      },
    },
    {
      _id: new ObjectId('60a019faa13450d1025f8041'),
      title: 'Dolor sit amet',
      text: '{"type":"doc","content":[{"type":"paragraph","content":[{"type":"text","text":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}]}]}',
      tale: new ObjectId('60a0188367e1c2e07fa5201d'),
      published: true,
      next: {
        choices: [{
          text: 'Consectetur adipiscing elit',
          chapter: new ObjectId('60a01a00057dd84474a4737c'),
        }, {
          text: 'Sed do eiusmod tempor',
          chapter: new ObjectId('60a01a073c4c60dccde24185'),
        }],
      },
    },
    {
      _id: new ObjectId('60a01a00057dd84474a4737c'),
      title: 'Consectetur adipiscing elit',
      text: '{"type":"doc","content":[{"type":"paragraph","content":[{"type":"text","text":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}]}]}',
      tale: new ObjectId('60a0188367e1c2e07fa5201d'),
      published: true,
    },
    {
      _id: new ObjectId('60a01a073c4c60dccde24185'),
      title: 'Sed do eiusmod tempor',
      text: '{"type":"doc","content":[{"type":"paragraph","content":[{"type":"text","text":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}]}]}',
      tale: new ObjectId('60a0188367e1c2e07fa5201d'),
      published: true,
      next: {
        chapter: new ObjectId('60a01ab5453b29a139f84ee3'),
      },
    },
    {
      _id: new ObjectId('60a01ab5453b29a139f84ee3'),
      title: 'incididunt ut labore et dolore magna aliqua',
      text: '{"type":"doc","content":[{"type":"paragraph","content":[{"type":"text","text":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}]}]}',
      tale: new ObjectId('60a0188367e1c2e07fa5201d'),
      published: true,
    },

    /* Lord Of The Rings */
    {
      _id: new ObjectId('609e61535860c41734c0bfb4'),
      title: 'Une réception depuis longtemps attendue',
      text: '{"type":"doc","content":[{"type":"paragraph","content":[{"type":"text","text":"Quand M. Bilbon Sacquet, de Cul-de-sac, annonça qu’il donnerait à l’occasion de son undécante-unième anniversaire une réception d’une magnificence particulière, une grande excitation régna dans Hobbitebourg, et toute la ville en parla. Bilbon était en même temps très riche et très particulier, et il avait fait l’étonnement de la Comté pendant soixante ans, c’est-à-dire depuis sa remarquable disparition et son retour inattendu. Les richesses qu’il avait rapportées de ses voyages étaient devenus une légende locale, et l’on croyait communément, en dépit des assurances des anciens, que la Colline de Cul-de-sac était creusée de galeries bourrées de trésors. Et si cela n’eût pas suffi à assurer sa renommée, sa vigueur prolongée aurait encore fait l’admiration de tous. Le temps s’écoulait, mais il semblait n’avoir aucune prise sur M. Sacquet. À quatre-vingt-dix ans, il était tout semblable à ce qu’il était à cinquante. À quatre-vingt-dix-neuf ans, on commença à le qualifier de bien conservé ; mais inchangé aurait été plus près de la vérité. D’aucuns hochaient la tête, pensant que c’était trop d’une bonne chose ; il paraissait injuste que quelqu’un pût jouir (visiblement) d’une jeunesse perpétuelle en même temps que (suivant l’opinion commune) d’une opulence inépuisable. — Cela aura sa contrepartie, disait-on. Ce n’est pas naturel, et il en viendra certainement des ennuis ! Mais jusque-là aucun ennui n’était venu ; et comme M. Sacquet était généreux de son argent, la plupart des gens lui pardonnaient volontiers ses singularités et sa bonne fortune. Il était en relations de visite avec ses parents (hormis, naturellement, les Sacquet de Besace) et il avait de nombreux admirateurs fervents parmi les Hobbits des familles pauvres et peu importantes. Mais il n’eut pas d’amis intimes jusqu’au moment où certains de ses jeunes cousins commencèrent à prendre de l’âge. L’aîné de ceux-ci et le favori de Bilbon était le jeune Frodon Sacquet. À quatre-vingt-dix-neuf ans, Bilbon l’adopta comme héritier ; il l’amena vivre à Cul-de-sac, et les espérances des Sacquet de Besace furent définitivement anéanties."}]}]}',
      tale: new ObjectId('5fd6498973b381029736b562'),
      published: true,
    },
  ],
};
