const ObjectId = require('mongodb').ObjectId;

module.exports = {
  collection: 'tales',
  documents: [
    {
      _id: new ObjectId('5fd6498973b381029736b561'),
      title: 'Harry Potter',
      description:
        "Le jour de ses onze ans, Harry Potter, un orphelin élevé par un oncle et une tante qui le détestent, voit son existence bouleversée. Un géant vient le chercher pour l'emmener à Poudlard, la célèbre école de sorcellerie où une place l'attend depuis toujours. Voler sur des balais, jeter des sorts, combattre des Trolls : Harry Potter se révèle un sorcier vraiment doué. Mais quel mystère entoure sa naissance et qui est l'effroyable V..., le mage dont personne n'ose prononcer le nom ?",
      author: new ObjectId('5fd6498973b381029736c783'),
      ended: false,
    },
    {
      _id: new ObjectId('5fd6498973b381029736b562'),
      title: 'Le seigneur des anneaux',
      description:
        "Un jeune et timide 'Hobbit', Frodon Sacquet, hérite d'un anneau magique. Bien loin d'être une simple babiole, il s'agit d'un instrument de pouvoir absolu qui permettrait à Sauron, le 'Seigneur des ténèbres', de régner sur la 'Terre du Milieu' et de réduire en esclavage ses peuples. Frodon doit parvenir jusqu'à la 'Crevasse du Destin' pour détruire l'anneau.",
      author: new ObjectId('5fd6498973b381029736c784'),
      ended: true,
    },
    {
      _id: new ObjectId('60a0188367e1c2e07fa5201d'),
      title: 'Divintel\'s tale',
      description: 'Just a simple tale.',
      author: new ObjectId('60a018ac7d6afd4afd50fc24'),
      ended: false,
    },
  ],
};
