const dbClient = require('../db/db_client');
const data = require('./index');

module.exports = {
  async setup() {
    await dbClient.db().dropDatabase();
    console.info('Database cleaned.');
    const inserts = data.map(
      (d) =>
        new Promise((resolve) => {
          return dbClient.db()
            .collection(d.collection)
            .insertMany(d.documents)
            .then(() => {
              console.info(
                `${d.documents.length} inserted documents into collection: ${d.collection}.`
              );
              resolve();
            });
        })
    );
    return Promise.all(inserts).then(() => {
      console.info('Database ready to use!');
    });
  },
};
