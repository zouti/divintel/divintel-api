const users = require('./users');
const tales = require('./tales');
const chapters = require('./chapters');

module.exports = [users, tales, chapters];
