const ObjectId = require('mongodb').ObjectId;

module.exports = {
  collection: 'users',
  documents: [
    {
      _id: new ObjectId('60a018ac7d6afd4afd50fc24'),
      login: 'Divintel',
    },
    {
      _id: new ObjectId('5fd6498973b381029736c783'),
      login: 'J.K Rowling',
    },
    {
      _id: new ObjectId('5fd6498973b381029736c784'),
      login: 'J.R.R. Tolkien',
    },
  ],
};
