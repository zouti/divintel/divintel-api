const { setup } = require('./seed');
const dbClient = require('../db/db_client');

dbClient.connect().then(() => {
  setup().then(() => {
    process.exit();
  });
});
