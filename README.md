# Divintel API

## Setup Docker

Create network _divintel_ if it does not exist :

```bash
docker network create divintel
```

Create and run container :

```bash
docker-compose up -d
```
