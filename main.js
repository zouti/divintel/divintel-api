const dbClient = require('./lib/db/db_client');
const { createServer } = require('./lib/server');

const start = async () => {
  await dbClient.connect();
  await createServer().catch((err) => {
    console.error(err);
    process.exit(1);
  });
};

start();
