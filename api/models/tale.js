/**
 * Tales business handling
 */
module.exports = {
  getTree (chapters) {
    const start = chapters.find(chapter => !chapters.some(c =>
      (c.next && c.next.chapter && c.next.chapter.toString() === chapter._id.toString()) ||
      (c.next && c.next.choices && c.next.choices.some(
        choice => choice.chapter.toString() === chapter._id.toString()
      ))
    ));
    
    chapters.forEach(chapter => {
      if (chapter.next) {
        if (chapter.next.chapter) {
          chapter.next.chapter = chapters.find(
            c => c._id.toString() === chapter.next.chapter.toString()
          );
        }
        if (chapter.next.choices) {
          chapter.next.choices.forEach(choice => {
            choice.chapter = chapters.find(
              c => c._id.toString() === choice.chapter.toString()
            );
          })
        }
      }
    })
    return start;
  }
};
