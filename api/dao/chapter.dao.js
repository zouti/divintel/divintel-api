const { db, ObjectId } = require('../../lib/db/db_client');
const { USERS, TALES, CHAPTERS } = require('../constants').collections;
const dao = require('./dao');

/**
 * Chapters store handling
 */
module.exports = {
  async findById(id) {
    try {
      return await db()
        .collection(CHAPTERS)
        .aggregate([
          { $match: { _id: ObjectId(id) } },
          { $lookup: {
            from: TALES,
            localField: 'tale',
            foreignField: '_id',
            as: 'tale'
          } },
          { $unwind: '$tale' },
          { $lookup: {
            from: USERS,
            localField: 'tale.author',
            foreignField: '_id',
            as: 'tale.author'
          } },
          { $unwind: '$tale.author' },
          { $project: {
            title: 1,
            text: 1,
            next: 1,
            published: 1,
            tale: {
              _id: 1,
              title: 1,
              description: 1,
              ended: 1,
              author: {
                _id: 1,
                login: 1
              },
            },
          } }
        ]).next();
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
  existAll: (ids) => dao.existAll(CHAPTERS, ids),
  exist: (id) => dao.exist(CHAPTERS, id),
  create: (chapter) => {
    chapter.tale = ObjectId(chapter.tale);
    return dao.create(CHAPTERS, chapter);
  },
  update: (id, chapter) => dao.update(CHAPTERS, id, chapter),
  remove: (id) => dao.remove(CHAPTERS, id),
};
