const { USERS } = require('../constants').collections;
const dao = require('./dao');

/**
 * Users store handling
 */
module.exports = {
  findById: (id) => dao.findById(USERS, id),
};
