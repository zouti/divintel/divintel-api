const { db, ObjectId } = require('../../lib/db/db_client');
const { USERS, TALES, CHAPTERS } = require('../constants').collections;
const dao = require('./dao');

/**
 * Tales store handling
 */
module.exports = {
  async findAll() {
    try {
      return await db()
        .collection(TALES)
        .aggregate([
          { $lookup: {
            from: USERS,
            localField: 'author',
            foreignField: '_id',
            as: 'author',
          } },
          { $unwind: '$author' },
          { $project: {
            title: 1,
            description: 1,
            author: 1,
            ended: 1,
          } },
        ])
        .toArray();
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
  async findById(id) {
    try {
      return await db()
        .collection(TALES)
        .aggregate([
          { $match: { _id: ObjectId(id) } },
          { $lookup: {
            from: USERS,
            localField: 'author',
            foreignField: '_id',
            as: 'author',
          } },
          { $unwind: '$author' },
          { $lookup: {
            from: CHAPTERS,
            localField: '_id',
            foreignField: 'tale',
            as: 'chapters',
          } },
          { $project: {
            title: 1,
            description: 1,
            author: 1,
            ended: 1,
            chapters: {
              _id: 1,
              title: 1,
              published: 1,
              next: 1,
            },
          } },
        ])
        .next();
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
  existAll: (ids) => dao.existAll(TALES, ids),
  exist: (id) => dao.exist(TALES, id),
  create: (tale) => dao.create(TALES, tale),
  update: (id, tale) => dao.update(TALES, id, tale),
  remove: (id) => dao.remove(TALES, id),
};
