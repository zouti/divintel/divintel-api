const { db, ObjectId } = require('../../lib/db/db_client');

/**
 * Common store handling
 */
module.exports = {
  async findById(collection, id) {
    try {
      return await db().collection(collection).findOne({ _id: ObjectId(id) });
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
  async existAll(collection, ids) {
    try {
      return (await db().collection(collection).countDocuments({
        _id: { $in: ids.map(id => ObjectId(id)) }
      })) === ids.length;
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
  async exist(collection, id) {
    try {
      return (await db().collection(collection).countDocuments({
        _id: ObjectId(id)
      })) === 1;
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
  async create(collection, item) {
    try {
      const { insertedId: _id } = await db().collection(collection).insertOne(item);
      return { _id, ...item };
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
  async update(collection, id, item) {
    try {
      const res = await db().collection(collection).updateOne({ _id: ObjectId(id) }, { $set: item });
      return res.modifiedCount === 1;
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
  async remove(collection, id) {
    try {
      const res = await db().collection(collection).deleteOne({ _id: ObjectId(id) });
      return res.deletedCount === 1;
    } catch (err) {
      console.error(err);
      throw err;
    }
  },
}