const { NotFound } = require('http-errors');
const UserDao = require('../dao/user.dao');

/**
 * Users routes handling
 */
module.exports = {
  async getUser(request, reply) {
    const id = request.params.id;
    const user = await UserDao.findById(id);
    if (!user) throw new NotFound();
    reply.send(user);
  },
};
