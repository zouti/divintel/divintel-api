const { NotFound, BadRequest } = require('http-errors');
const { isDefined } = require('../../lib/utils');
const TaleDao = require('../dao/tale.dao');
const ChapterDao = require('../dao/chapter.dao');

/**
 * Chapters routes handling
 */
module.exports = {
  async getChapter(request, reply) {
    const id = request.params.id;
    const chapter = await ChapterDao.findById(id);
    if (!chapter) throw new NotFound();
    reply.send(chapter);
  },
  async createChapter(request, reply) {
    const taleId = request.body.tale;
    if (!(await TaleDao.exist(taleId)))
      throw new BadRequest('Tale does not exist.');
    const result = await ChapterDao.create({ ...request.body, published: false });
    reply.send(result);
  },
  async updateChapter(request, reply) {
    const id = request.params.id;
    const { title, text, published } = request.body;
    const chapter = {};
    if (isDefined(title)) chapter.title = title;
    if (isDefined(text)) chapter.text = text;
    if (isDefined(published)) chapter.published = published;
    const res = await ChapterDao.update(id, chapter);
    if (!res) throw new NotFound();
    reply.send(res);
  },
  async linkChapter(request, reply) {
    const id = request.params.id;
    const { chapter: chapterId, choices } = request.body;
    const chapter = {};
  
    if (chapterId) {
      if (!(await ChapterDao.exist(chapterId)))
        throw new BadRequest('Linked chapter does not exist.');
      chapter.next = { chapter: chapterId };
    } else if (choices) {
      if (!(await ChapterDao.existAll(choices.map(choice => choice.chapter))))
        throw new BadRequest('Some linked chapters does not exist.');
      chapter.next = { choices };
    }
  
    const res = await ChapterDao.update(id, chapter);
    if (!res) throw new NotFound();
    reply.send(res);
  },
  async removeChapter(request, reply) {
    const id = request.params.id;
    const res = await ChapterDao.remove(id);
    if (!res) throw new NotFound();
    reply.send(res);
  },
};
