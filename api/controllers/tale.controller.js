const { NotFound } = require('http-errors');
const { isDefined } = require('../../lib/utils');
const TaleDao = require('../dao/tale.dao');
const Tale = require('../models/tale');

/**
 * Tales routes handling
 */
module.exports = {
  async getTales(request, reply) {
    const tales = await TaleDao.findAll();
    reply.send(tales);
  },
  async getTale(request, reply) {
    const id = request.params.id;
    const tale = await TaleDao.findById(id);
    if (!tale) throw new NotFound();
    tale.chapters = Tale.getTree(tale.chapters);
    reply.send(tale);
  },
  async createTale(request, reply) {
    const res = await TaleDao.create({ ...request.body, ended: false });
    reply.send(res);
  },
  async updateTale(request, reply) {
    const id = request.params.id;
    const { title, description, ended } = request.body;
    const tale = {};
    if (isDefined(title)) tale.title = title;
    if (isDefined(description)) tale.description = description;
    if (isDefined(ended)) tale.ended = ended;
    const res = await TaleDao.update(id, tale);
    if (!res) throw new NotFound();
    reply.send(res);
  },
  async removeTale(request, reply) {
    const id = request.params.id;
    const res = await TaleDao.remove(id);
    if (!res) throw new NotFound();
    reply.send(res);
  },
};
