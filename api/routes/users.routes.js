const { getUser } = require('../controllers/user.controller');

module.exports = [
  {
    method: 'GET',
    url: '/users/:id',
    schema: {
      tags: ['users'],
      response: {
        200: { $ref: 'User' }
      },
    },
    handler: getUser
  },
];