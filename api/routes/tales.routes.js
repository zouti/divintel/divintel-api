const { getTales, getTale, createTale, updateTale, removeTale } = require('../controllers/tale.controller');

module.exports = [
  {
    method: 'GET',
    url: '/tales',
    schema: {
      tags: ['tales'],
      response: {
        200: {
          type: 'array',
          items: { $ref: 'Tale' },
        }
      },
    },
    handler: getTales
  },

  {
    method: 'POST',
    url: '/tales',
    schema: {
      tags: ['tales'],
      body: {
        type: 'object',
        required: ['title', 'description'],
        properties: {
          title: { type: 'string' },
          description: { type: 'string' }
        }
      },
      response: {
        200: { $ref: 'Tale' },
      },
    },
    handler: createTale
  },

  {
    method: 'GET',
    url: '/tales/:id',
    schema: {
      tags: ['tales'],
      params: {
        type: 'object',
        properties: {
          id: { $ref: 'ObjectId' },
        }
      },
      response: {
        200: { $ref: 'CompleteTale' },
      },
    },
    handler: getTale
  },

  {
    method: 'PUT',
    url: '/tales/:id',
    schema: {
      tags: ['tales'],
      params: {
        type: 'object',
        properties: {
          id: { $ref: 'ObjectId' },
        }
      },
      body: {
        type: 'object',
        properties: {
          title: { type: 'string' },
          description: { type: 'string' },
          ended: { type: 'boolean' }
        }
      },
      response: {
        200: { type: 'boolean' },
      },
    },
    handler: updateTale
  },

  {
    method: 'DELETE',
    url: '/tales/:id',
    schema: {
      tags: ['tales'],
      params: {
        type: 'object',
        properties: {
          id: { $ref: 'ObjectId' },
        }
      },
      response: {
        200: { type: 'boolean' },
      },
    },
    handler: removeTale
  },
];