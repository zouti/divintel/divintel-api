module.exports = app => {
  app.addSchema({
    $id: 'ObjectId',
    type: 'string',
    minLength: 24,
    maxLength: 24,
    pattern: '^[0-9a-fA-F]{24}$',
    example: '609e6f96726a5c5107699082',
  });
};
