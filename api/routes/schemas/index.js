module.exports = app => {
  require('./objectid.schema')(app);
  require('./user.schema')(app);
  require('./tale.schema')(app);
  require('./chapter.schema')(app);
  require('./link.schema')(app);
};