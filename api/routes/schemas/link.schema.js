const buildSchema = ($id, chapter) => ({
  $id,
  oneOf: [
    {
      type: 'object',
      required: ['chapter'],
      properties: {
        chapter,
      },
    },
    {
      type: 'object',
      required: ['choices'],
      properties: {
        choices: {
          type: "array",
          minItems: 2,
          items: {
            type: 'object',
            properties: {
              text: { type: 'string' },
              chapter,
            },
            required: ['text', 'chapter'],
          },
        },
      },
    },
  ],
});

module.exports = app => {
  app.addSchema(buildSchema('Link', { $ref: 'ObjectId' }));
  app.addSchema(buildSchema('ResolvedLink', { /* Empty object because of circular schema */ }));
};