module.exports = app => {
  app.addSchema({
    $id: 'Chapter',
    type: 'object',
    properties: {
      _id: { $ref: 'ObjectId' },
      title: { type: 'string' },
      text: { type: 'string' },
      published: { type: 'boolean' },
      tale: { $ref: 'ObjectId' },
    },
  });
  
  app.addSchema({
    $id: 'CompleteChapter',
    type: 'object',
    properties: {
      _id: { $ref: 'Chapter#/properties/_id' },
      title: { $ref: 'Chapter#/properties/title' },
      text: { $ref: 'Chapter#/properties/text' },
      published: { $ref: 'Chapter#/properties/published' },
      tale: { $ref: 'Tale' },
      next: { $ref: 'Link' },
    },
  });
  
  app.addSchema({
    $id: 'ResolvedChapter',
    type: 'object',
    properties: {
      _id: { $ref: 'Chapter#/properties/_id' },
      title: { $ref: 'Chapter#/properties/title' },
      published: { $ref: 'Chapter#/properties/published' },
      next: { $ref: 'ResolvedLink' },
    },
  });
};