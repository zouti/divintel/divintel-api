module.exports = app => {
  app.addSchema({
    $id: 'User',
    type: 'object',
    properties: {
      _id: { $ref: 'ObjectId' },
      login: { type: 'string' },
    },
  });
};