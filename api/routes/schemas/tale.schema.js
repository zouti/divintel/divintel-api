module.exports = app => {
  app.addSchema({
    $id: 'Tale',
    type: 'object',
    properties: {
      _id: { $ref: 'ObjectId' },
      title: { type: 'string' },
      description: { type: 'string' },
      author: { $ref: 'User' },
      ended: { type: 'boolean' },
    },
  });
  
  app.addSchema({
    $id: 'CompleteTale',
    type: 'object',
    properties: {
      _id: { $ref: 'Tale#/properties/_id' },
      title: { $ref: 'Tale#/properties/title' },
      description: { $ref: 'Tale#/properties/description' },
      author: { $ref: 'Tale#/properties/author' },
      ended: { $ref: 'Tale#/properties/ended' },
      chapters: { $ref: 'ResolvedChapter' },
    },
  });
};