const { getChapter, createChapter, updateChapter, linkChapter, removeChapter } = require('../controllers/chapter.controller');

module.exports = [
  {
    method: 'POST',
    url: '/chapters',
    schema: {
      tags: ['chapters'],
      body: {
        type: 'object',
        required: ['title', 'text', 'tale'],
        properties: {
          title: { type: 'string' },
          text: { type: 'string' },
          tale: { $ref: 'ObjectId' }
        }
      },
      response: {
        200: { $ref: 'Chapter' }
      },
    },
    handler: createChapter
  },

  {
    method: 'GET',
    url: '/chapters/:id',
    schema: {
      tags: ['chapters'],
      params: {
        type: 'object',
        properties: {
          id: { $ref: 'ObjectId' },
        }
      },
      response: {
        200: { $ref: 'CompleteChapter' },
      },
    },
    handler: getChapter
  },

  {
    method: 'PUT',
    url: '/chapters/:id',
    schema: {
      tags: ['chapters'],
      params: {
        type: 'object',
        properties: {
          id: { $ref: 'ObjectId' },
        }
      },
      body: {
        type: 'object',
        properties: {
          title: { type: 'string' },
          text: { type: 'string' },
          published: { type: 'boolean' },
        }
      },
      response: {
        200: { type: 'boolean' },
      },
    },
    handler: updateChapter
  },

  {
    method: 'PATCH',
    url: '/chapters/:id',
    schema: {
      tags: ['chapters'],
      params: {
        type: 'object',
        properties: {
          id: { $ref: 'ObjectId' },
        }
      },
      body: { $ref: 'Link' },
      response: {
        200: { type: 'boolean' },
      },
    },
    handler: linkChapter
  },

  {
    method: 'DELETE',
    url: '/chapters/:id',
    schema: {
      tags: ['chapters'],
      params: {
        type: 'object',
        properties: {
          id: { $ref: 'ObjectId' },
        }
      },
      response: {
        200: { type: 'boolean' },
      },
    },
    handler: removeChapter
  },
];