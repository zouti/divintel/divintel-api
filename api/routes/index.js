const fastifySwagger = require('fastify-swagger');
const routes = [
  ...require('./users.routes'),
  ...require('./tales.routes'),
  ...require('./chapters.routes'),
].flat();

module.exports = fastify => {
  fastify.register(fastifySwagger, {
    routePrefix: '/docs',
    openapi: {
      info: {
        title: 'Divintel API',
        description: 'Docs of Divintel API',
        version: '0.1.0'
      },
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: [
        { name: 'users', description: 'Users related end-points' },
        { name: 'tales', description: 'Tales related end-points' },
        { name: 'chapters', description: 'Chapters related end-points' }
      ],
      paths: routes,
    },
    exposeRoute: true,
  });

  fastify.register(async app => {
    routes.forEach(route => app.route(route));
  }, { prefix: '/api/v1' });
}